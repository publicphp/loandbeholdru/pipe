<?php

namespace loandbeholdru\pipe;

class pipe
{
    const METH = "aes128";
    public static function exec(pipecommand $command, piperesult $result, \Exception $oncommand = null, int $delay = 1)
    {
        fwrite($command->pipe($result->onpipe()), "$command");
        sleep($delay);
        return $result->read($command->unic(), $oncommand);
    }
    public static function example_hash(){
        return hash('md5', file_get_contents(
            static::classpath() . '/example/docker-compose.yml'
        ));
    }
    public static function classpath(){
        return __DIR__ ;
    }
}