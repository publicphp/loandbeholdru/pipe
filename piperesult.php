<?php

namespace loandbeholdru\pipe;

class piperesult
{
    protected $onpipe;
    protected $path;
    public function __construct(string $path, \Exception $onpipe = null)
    {
        $this->onpipe = $onpipe ?? new \Exception("Pipe not working!");
        $this->path = $path;

    }

    public function onpipe()
    {
        return $this->onpipe;
    }

    public function read(string $unic, $classORobject = null)
    {
        $classORobject = $classORobject ?? new \Exception();

        $cl = is_string($classORobject)
            ? $classORobject : get_class($classORobject);

        $result = sprintf('%s/%s', $this->path, $unic);

        if (realpath("$result.err")){
            $err = file_get_contents("$result.err");
            unlink("$result.err");
            throw new $cl($err);
        }

        if (!realpath("$result"))
            throw $this->onpipe();

        $resp = file_get_contents($result);


        return $resp;
    }

}