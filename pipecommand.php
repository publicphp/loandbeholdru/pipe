<?php

namespace loandbeholdru\pipe;


class pipecommand
{
    protected static $pipes = [];
    protected $pipename;
    protected $command;
    protected $unic;

    /**
     * @param $pipename
     * @param $template
     */
    public function __construct(string $pipename, string $template, string ...$params)
    {
        $this->pipename = $pipename;
        $this->command = sprintf("$template\n",  ...$params);
    }

    public function unic()
    {
        $data = explode(' ', $this->command);
        $nic = array_shift($data);
        $nic = str_replace("\n",'',$nic);

        return $this->unic = $this->unic
            ?? uniqid(sprintf("%s_", $nic));
    }

    public function pipe(\Exception $onpipe = null)
    {
        try{
            static::$pipes[$this->pipename] = static::$pipes[$this->pipename]
                ?? fopen($this->pipename, 'w');
        }catch (\Exception $e){
            throw ($onpipe ?? $e);
        }
        return static::$pipes[$this->pipename];
    }

    public function __toString(){
        return sprintf("%s %s", $this->unic(), $this->command);
    }
    
    public function __destruct()
    {
        if (!empty(static::$pipes[$this->pipename])){
            fclose(static::$pipes[$this->pipename]);
            static::$pipes[$this->pipename] = null;
        }
    }

}