#!/bin/bash

date >> /tmp/serve

# pipe
PIPE=${1:-/opt/pipes/vpnpipe}

# reply dir
PTH=${2:-/opt/files}

# Watch if scripts end with error or normal done
function watcher() {
  FILE=$1
  WAIT=${2:-4}
  sleep $WAIT
  [ -e "$FILE" ] && return
  [ -e "$FILE.err" ] && return
  echo "Script not started!" > "$FILE.err"
}

# Rename error file (created by default) to normal done reaction
function reporter() {
  ERR="$1"
  DONE="${2:-$ERR}"
  [ -e "$ERR" ] && mv "$ERR" "$DONE" || echo "Finished without report" > "$ERR"
}

# Cleaning reaction shadow for each command execution after 50 secs
function clean() {
    sleep 50
    rm "$1$2"
    rm "$1$3"
}

# Execute command
function executer() {
    UNIC=$1
    COMMAND="$2"
    OPTS="$3"
    otherOPTS="$4"

    ERR="$PTH/$UNIC.err"
    DONE="$PTH/$UNIC"

    echo "UNIC: $UNIC COMMAND: $COMMAND OPTS: $OPTS otherOPTS: $otherOPTS"
    case $COMMAND in
    rules)
      iptables-save > "$DONE"
      ;;
    *)
      echo "Command '$COMMAND' does't exist." > "$ERR"
      ;;
    esac
}
echo "$PIPE|"  >> /tmp/serve

# Execution while
while true; do
    read OPTS < "$PIPE"
    echo "$OPTS" >> /tmp/serve
    UNIC=$(echo "$OPTS" | awk '{print $1}')
    echo "$UNIC"
    (watcher "$PTH/$UNIC" &)
    executer $OPTS
    (clean "$PTH/$UNIC" "" ".err" &)
done








