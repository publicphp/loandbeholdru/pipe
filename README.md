# pipe

It's small and lightweight pack to run shell script throw command pipe (bus).
So useful to build webhooks on top of docker-container with shell scripts and link them with REST Api.

You no longer need to learn fancy package configurations for webhooks. Use the usual tools.

## Installation

It's recommended that you use [Composer](https://getcomposer.org/) to install shorts.

```bash
$ composer require loandbeholdru/pipe
```

## Usage

Create pipe on host with php and give rights to read it on host with executing scripts ([docker-composer example](https://git.1qr.org/publicphp/loandbeholdru/pipe/-/raw/master/example/docker-compose.yml)):
```shell
mkfifo /tmp/pipefile
```

On host/container with shell-scripts run main execution [script](https://git.1qr.org/publicphp/loandbeholdru/pipe/-/raw/master/example/serve.sh):
```shell
./serve.sh [path_to_pipe] [path_to_reaction_dir]
```

Create command:
```php
$command = new pipecommand("/tmp/pipefile", "/some/path/script_with_two_params.sh %s %s", $par1, $par2);
```

Create result trap:  
```php
$result = new piperesult("/tmp/pipefile", new brokenPipeException("When pipe not working!"));
```

Execute your command and get $strings from your shell-script:  
```php
$strings = pipe::exec($command, $result, new bashCommandErrorException("When your script return error!"));
```

## Contributions

... are always welcome. Many times it is useful to just point out a use case the author have not thought about or come across.


